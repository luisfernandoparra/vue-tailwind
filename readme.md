<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About the project

Following the course https://nick-basile.com/blog/post/setting-up-tailwind-in-a-laravel-project, let's implement a simple Vue + TailwindCSS + Laravel Project. 

Html + CSS from TailWindCSS

BackEnd Laravel

## Requirements

Vue and Bulma Requirements
```bash
    npm install
    npm install laravel-mix --save-dev
    cp -r node_modules/laravel-mix/setup/** ./
    npm install tailwindcss --save-dev
    

    
```

Laravel and DB Requirements
DB must be defined at the *.env* file
```bash
    php artisan migrate
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
